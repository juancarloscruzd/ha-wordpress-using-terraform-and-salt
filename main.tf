variable "aws_access_key" {
}

variable "aws_secret_key" {
}

variable "ec2_private_key" {
}

variable "environment_name" {
}

variable "azs" {
  type = list(string)
}

variable "database_subnets" {
  type = list(string)
}

variable "public_subnets" {
  type = list(string)
}

variable "app_name" {
}

variable "app_name_slug" {
}

variable "bucket_name" {
}

variable "dns_root" {
}

variable "wp_admin_user" {
}

variable "wp_admin_password" {
}

variable "wp_site_title" {
}

variable "wp_site_email" {
}

variable "http_protocol" {
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = "eu-west-2"
}

module "network" {
  source           = "./modules/network/"
  azs              = var.azs
  database_subnets = var.database_subnets
  public_subnets   = var.public_subnets
  environment_name = var.environment_name
}

module "media" {
  source           = "./modules/media/"
  app_name         = var.app_name
  environment_name = var.environment_name
  bucket_name      = var.bucket_name
  dns_root         = var.dns_root
}

module "efs" {
  source           = "./modules/efs/"
  vpc_id           = module.network.vpc_id
  public_subnets   = module.network.public_subnets
  environment_name = var.environment_name
}

module "database" {
  source           = "./modules/database/"
  vpc_id           = module.network.vpc_id
  db_subnet_group  = module.network.db_subnet_group_name
  environment_name = var.environment_name
}

module "compute" {
  source           = "./modules/compute/"
  vpc_id           = module.network.vpc_id
  public_subnets   = module.network.public_subnets
  environment_name = var.environment_name
  efs_host         = module.efs.efs_host
  dns_root         = var.dns_root
  seeder_host      = module.seeder.seeder_host
}

module "seeder" {
  source            = "./modules/seeder/"
  vpc_id            = module.network.vpc_id
  public_subnets    = module.network.public_subnets
  environment_name  = var.environment_name
  db_endpoint       = module.database.endpoint
  db_user           = module.database.username
  db_pass           = module.database.password
  efs_host          = module.efs.efs_host
  efs_mounts        = module.efs.efs_mounts
  wp_admin_user     = var.wp_admin_user
  wp_admin_password = var.wp_admin_password
  wp_site_title     = var.wp_site_title
  wp_site_email     = var.wp_site_email
  http_protocol     = var.http_protocol
  dns_root          = var.dns_root
  ec2_private_key   = var.ec2_private_key
}

