variable "environment_name" {
}

variable "azs" {
  type = list(string)
}

variable "database_subnets" {
  type = list(string)
}

variable "public_subnets" {
  type = list(string)
}

data "aws_security_group" "default" {
  name   = "default"
  vpc_id = module.vpc.vpc_id
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.15.0"
  name    = "wordpress-ha-vpc"
  cidr    = "10.0.0.0/16"

  azs              = var.azs
  database_subnets = var.database_subnets
  public_subnets   = var.public_subnets

  enable_nat_gateway = false
  enable_vpn_gateway = false
  enable_s3_endpoint = true

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Terraform   = "true"
    Environment = var.environment_name
  }
}

