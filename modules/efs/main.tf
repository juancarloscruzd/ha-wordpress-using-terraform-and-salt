data "aws_subnet_ids" "efs_subnets" {
  vpc_id = var.vpc_id

  tags = {
    Terraform   = "true"
    Environment = var.environment_name
  }
}

resource "aws_security_group" "sg-EFS" {
  name   = "WordPress - EFS"
  vpc_id = var.vpc_id

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_efs_file_system" "wproot-efs" {
}

resource "aws_efs_mount_target" "wproot-efs-mounts" {
  subnet_id       = element(var.public_subnets, count.index)
  file_system_id  = aws_efs_file_system.wproot-efs.id
  security_groups = [aws_security_group.sg-EFS.id]
  count           = length(var.public_subnets)
}

