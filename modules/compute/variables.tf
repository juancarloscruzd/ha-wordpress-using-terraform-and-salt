variable "public_subnets" {
  type = list(string)
}

variable "environment_name" {
}

variable "vpc_id" {
}

variable "efs_host" {
}

variable "dns_root" {
}

variable "seeder_host" {
}

