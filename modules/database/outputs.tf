output "endpoint" {
  value = module.rds.this_db_instance_address
}

output "username" {
  value = module.rds.this_db_instance_username
}

output "password" {
  value = module.rds.this_db_instance_password
}

