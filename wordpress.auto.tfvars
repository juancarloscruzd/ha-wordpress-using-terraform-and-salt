# Tag should change depending on your environment
environment_name = "test" 
# Change to your local Availability Zones
azs = ["eu-west-2a", "eu-west-2b", "eu-west-2c"] 
database_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"] 
public_subnets = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"] 
# This is unique across AWS so will need changing to your chosen bucket na,e
bucket_name = "wordpressha-terraform-dev"
app_name = "WordPress HA with Terraform"
app_name_slug = "wordpress_ha_with_terraform"
# For example nextamazing.site
dns_root = "nextamazing.site"
# WordPress Config
wp_admin_user = "nextamazingadmin" 
wp_admin_password = "ChANGeMe"
wp_site_title = "Next Amazing Site"
wp_site_email = "site@nextamzing.site"
http_protocol = "http"